In this simple 2D platform game player can move using [A] and [D] keys, jump with [Space] and attack enemies with [F].
Player can pick up coins and orbs. Orbs are used to open doors.
If players hearts reach 0, he dies.

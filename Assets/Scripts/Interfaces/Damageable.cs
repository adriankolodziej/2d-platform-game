﻿namespace Assets.Scripts.Interfaces
{
	interface Damageable
	{
		void OnDamageTaken();
	}
}

﻿using Assets.Scripts.Game;
using Assets.Scripts.Others;
using Assets.Scripts.Player;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
	public class HUDOrbsController : MonoBehaviour
	{
		[SerializeField] TextMeshProUGUI textField;
		[SerializeField] PlayerController playerController;
		private DigitsForHUD digitsForHUD;
		int orbs;
		string sOrbs;
		void Start()
		{
			EventSystem.current.onOrbPickUp += OnOrbPickUp;
			EventSystem.current.onOrbUse += OnOrbUse;
			digitsForHUD = new DigitsForHUD();
		}

		void OnOrbPickUp()
		{
			orbs = playerController.GetOrbKeysAmount();
			sOrbs = orbs.ToString();
			textField.text = "";
			digitsForHUD.CreateText(sOrbs, textField);
		}

		void OnOrbUse()
		{
			orbs = playerController.GetOrbKeysAmount();
			sOrbs = orbs.ToString();
			textField.text = "";
			digitsForHUD.CreateText(sOrbs, textField);
		}
	}
}
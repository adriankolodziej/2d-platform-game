﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
	public class CameraController : MonoBehaviour
	{
		[SerializeField] private float XAxisOffset;
		[SerializeField] private float MinX;
		[SerializeField] private float MaxX;
		private CharacterController2D characterController;
		void Start()
		{
			characterController = FindObjectOfType<CharacterController2D>();
		}

		// Update is called once per frame
		void Update()
		{
			if (characterController != null)
			{
				float newX = characterController.transform.position.x + XAxisOffset;
				if (newX <= MinX)
				{
					newX = MinX;
				}
				else if (newX >= MaxX)
					newX = MaxX;
				transform.position = new Vector3(newX, transform.position.y, transform.position.z);
			}
		}
	}
}
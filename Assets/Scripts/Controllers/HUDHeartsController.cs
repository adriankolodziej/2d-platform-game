﻿using Assets.Scripts.Game;
using Assets.Scripts.Player;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
	public class HUDHeartsController : MonoBehaviour
	{
		[SerializeField] private PlayerController controller;
		[SerializeField] private GameObject[] Hearts;
		[SerializeField] private GameObject[] EmptyHearts;
		private int maxHearts;
		private int currentHearts;
		void Start()
		{
			EventSystem.current.onPlayerDamageTaken += OnHeartLoss;
			EventSystem.current.onHeastRestore += OnHeartRestore;
			maxHearts = controller.GetMaxHearts();
			currentHearts = controller.GetCurrentHearts();
			UpdateHearts();
		}

		void UpdateHearts()
		{
			for (int i = 0; i < Hearts.Length; i++)
			{
				if (i > maxHearts - 1)
				{
					Hearts[i].SetActive(false);
					EmptyHearts[i].SetActive(false);
				}
				else
				{
					Hearts[i].SetActive(true);
					EmptyHearts[i].SetActive(true);
				}
				if (i > currentHearts - 1)
				{
					Hearts[i].SetActive(false);
				}
				else
				{
					Hearts[i].SetActive(true);
					Hearts[i].GetComponent<Animator>().SetTrigger("HeartRestore");
				}
			}
		}

		void OnHeartLoss()
		{
			currentHearts--;
			if(currentHearts>=0)
				Hearts[currentHearts].GetComponent<Animator>().SetTrigger("HeartLoss");
			StartCoroutine(UpdateHeartsCoroutine());
		}

		void OnHeartRestore()
		{
			if (currentHearts < maxHearts)
			{
				currentHearts++;
				UpdateHearts();
			}
		}
		IEnumerator UpdateHeartsCoroutine()
		{
			yield return new WaitForSeconds(0.5f);
			UpdateHearts();
		}
	}
}
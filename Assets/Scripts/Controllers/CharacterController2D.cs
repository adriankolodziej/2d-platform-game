﻿using Assets.Scripts.Enemies;
using Assets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Controllers
{
	public class CharacterController2D : MonoBehaviour
	{
		[SerializeField] private float JumpForce = 140f;
		[SerializeField] private float KnockBackForce = 50f;
		[Range(0, 0.4f)] [SerializeField] private float MovementSmoothing = .05f;
		[SerializeField] private bool AirControl = false;
		[SerializeField] private LayerMask GroundMask;
		[SerializeField] private Transform GroundCheck;
		[SerializeField] private Transform AttackCollisionCheck;

		private const float attackRange = 0.08f;
		private const float groundedRadius = 0.08f;
		private bool isGrounded;
		private new Rigidbody2D rigidbody2D;
		private bool isFacingRight = true;
		private Vector2 velocity = Vector2.zero;
		private bool hasAttacked = false;
		public UnityEvent OnLandEvent;
		[System.Serializable]
		public class BoolEvent : UnityEvent<bool> { }

		private void Awake()
		{
			rigidbody2D = GetComponent<Rigidbody2D>();

			if (OnLandEvent == null)
				OnLandEvent = new UnityEvent();
		}

		void FixedUpdate()
		{
			bool wasGrounded = isGrounded;
			isGrounded = false;

			Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, groundedRadius, GroundMask);
			for (int i = 0; i < colliders.Length; i++)
			{
				if (colliders[i].gameObject != gameObject)
				{
					isGrounded = true;
					if (!wasGrounded)
						OnLandEvent.Invoke();
				}
			}
		}

		public void Move(float move, bool jump)
		{
			if (isGrounded || AirControl)
			{
				Vector2 targetVelocity = new Vector2(move * 10f, rigidbody2D.velocity.y);
				rigidbody2D.velocity = Vector2.SmoothDamp(rigidbody2D.velocity, targetVelocity, ref velocity, MovementSmoothing);

				if (move > 0 && !isFacingRight)
				{
					Flip();
				}

				else if (move < 0 && isFacingRight)
				{
					Flip();
				}
			}

			if (isGrounded && jump)
			{
				isGrounded = false;
				rigidbody2D.AddForce(new Vector2(0f, JumpForce));
			}
		}

		void Flip()
		{
			isFacingRight = !isFacingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}

		public void Attack()
		{
			
			Collider2D[] colliders = Physics2D.OverlapCircleAll(AttackCollisionCheck.position, attackRange);
			for (int i = 0; i < colliders.Length; i++)
			{
				if (colliders[i].gameObject.GetComponent<EnemyController2D>() && !hasAttacked)
				{
					colliders[i].GetComponent<Damageable>().OnDamageTaken();
					if(transform.position.x < colliders[i].transform.position.x)
						colliders[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(KnockBackForce, KnockBackForce));
					else
						colliders[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(-KnockBackForce, KnockBackForce));
					hasAttacked = true;
				}
			}
			hasAttacked = false;
		}
	}
}
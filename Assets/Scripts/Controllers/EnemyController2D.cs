﻿using Assets.Scripts.Game;
using Assets.Scripts.Interfaces;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
	public class EnemyController2D : MonoBehaviour
	{
		[Range(0, 0.4f)] [SerializeField] private float MovementSmoothing = .05f;
		[SerializeField] private float KnockBackForce = 50f;
		[SerializeField] private LayerMask GroundMask;
		[SerializeField] private Transform GroundCheck;
		[SerializeField] private Transform AttackCollisionCheck;
		[SerializeField] private bool isFacingRight = true;

		private const float attackRange = 0.08f;
		private const float groundedRadius = 0.08f;
		private bool isGrounded;
		private new Rigidbody2D rigidbody2D;
		private Vector2 velocity = Vector2.zero;
		private bool hasAttacked;
		private void Awake()
		{
			rigidbody2D = GetComponent<Rigidbody2D>();
		}

		private void Start()
		{
			if (!isFacingRight)
				Flip();
		}
		void FixedUpdate()
		{

			bool wasGrounded = isGrounded;
			isGrounded = false;

			Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, groundedRadius, GroundMask);
			for (int i = 0; i < colliders.Length; i++)
			{
				if (colliders[i].gameObject != gameObject)
				{
					isGrounded = true;
				}
			}
		}
		public void Move(float move)
		{
			if (isGrounded)
			{
				Vector2 targetVelocity = new Vector2(move * 10f, rigidbody2D.velocity.y);
				rigidbody2D.velocity = Vector2.SmoothDamp(rigidbody2D.velocity, targetVelocity, ref velocity, MovementSmoothing);

				if (move > 0 && !isFacingRight)
				{
					Flip();
				}
				else if (move < 0 && isFacingRight)
				{
					Flip();
				}
			}
		}

		void Flip()
		{
			isFacingRight = !isFacingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}

		public void Attack(int direction)
		{
			if (isFacingRight && direction == -1)
				Flip();
			else if (!isFacingRight && direction == 1)
				Flip();

			Collider2D[] colliders = Physics2D.OverlapCircleAll(AttackCollisionCheck.position, attackRange);
			for (int i = 0; i < colliders.Length; i++)
			{
				if (colliders[i].gameObject.GetComponent<CharacterController2D>() && !hasAttacked)
				{
					if (transform.position.x < colliders[i].transform.position.x)
						colliders[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(KnockBackForce, KnockBackForce));
					else
						colliders[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(-KnockBackForce, KnockBackForce));
					EventSystem.current.PlayerDamageTaken();
					hasAttacked = true;				
				}
			}
			hasAttacked = false;
		}
	}
}
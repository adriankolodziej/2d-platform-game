﻿using Assets.Scripts.Game;
using Assets.Scripts.Others;
using Assets.Scripts.Player;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
	
	public class HUDCoinsController : MonoBehaviour
	{
		[SerializeField] TextMeshProUGUI textField;
		[SerializeField] PlayerController playerController;
		private DigitsForHUD digitsForHUD;
		int coins;
		string sCoins;
		void Start()
		{
			EventSystem.current.onCoinPickUp += OnCoinPickUp;
			digitsForHUD = new DigitsForHUD();
		}

		int GetCoinsAmount()
		{
			return playerController.GetCoinsAmount() + 1;
		}

		void OnCoinPickUp()
		{
			coins = GetCoinsAmount();
			sCoins = coins.ToString();
			textField.text = "";
			digitsForHUD.CreateText(sCoins, textField);
		}
	}
}
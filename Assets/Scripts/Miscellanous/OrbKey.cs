﻿using Assets.Scripts.Game;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Miscellanous
{
	public class OrbKey : BasePickUp
	{
		protected override void PickUp()
		{
			EventSystem.current.OrbPickUp();
			animator.SetBool("IsPickedUp", true);
			StartCoroutine(Destroy());
		}
	}
}
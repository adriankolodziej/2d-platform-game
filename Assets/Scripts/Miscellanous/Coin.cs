﻿using Assets.Scripts.Controllers;
using Assets.Scripts.Game;
using Assets.Scripts.Player;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Miscellanous
{
	public class Coin : BasePickUp
	{
		protected override void PickUp()
		{
			EventSystem.current.CoinPickUp();
			animator.SetBool("IsPickedUp", true);
			StartCoroutine(Destroy());
		}
	}
}
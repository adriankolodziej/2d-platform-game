﻿using Assets.Scripts.Controllers;
using Assets.Scripts.Game;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Miscellanous
{
	public abstract class BasePickUp : MonoBehaviour
	{
		[SerializeField] protected Animator animator;
		protected bool hasBeenPickedUp = false;

		protected void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.gameObject.GetComponent<CharacterController2D>())
			{
				if (!hasBeenPickedUp)
				{
					hasBeenPickedUp = true;
					PickUp();
				}
			}
		}

		abstract protected void PickUp();

		protected IEnumerator Destroy()
		{
			yield return new WaitForSeconds(0.5f);
			Destroy(gameObject);
		}
	}
}
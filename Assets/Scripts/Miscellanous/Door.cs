﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Miscellanous
{
	public class Door : MonoBehaviour
	{
		[SerializeField] private Animator animator;
		private bool isOpened = false;

		public bool GetIsOpened() { return isOpened; }
		public void Open()
		{
			if(!isOpened)
			{
				animator.SetTrigger("Open");
				StartCoroutine(DestroyThis());
				isOpened = true;
			}
		}

		IEnumerator DestroyThis()
		{
			yield return new WaitForSeconds(0.5f);
			Destroy(gameObject);
		}
	}
}
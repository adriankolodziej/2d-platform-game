﻿using Assets.Scripts.Game;
using Assets.Scripts.Player;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Miscellanous
{
	public class HealthPotion : MonoBehaviour
	{
		private bool hasBeenUsed = false;
		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (!hasBeenUsed)
			{
				if (collision.gameObject.GetComponent<PlayerController>())
				{
					if (collision.gameObject.GetComponent<PlayerController>().GetCurrentHearts() < collision.gameObject.GetComponent<PlayerController>().GetMaxHearts())
					{
						EventSystem.current.HeartRestore();
						hasBeenUsed = true;
						Destroy(gameObject);
					}
				}
			}
		}
	}
}
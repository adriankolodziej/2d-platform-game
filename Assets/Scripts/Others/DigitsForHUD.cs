﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;

namespace Assets.Scripts.Others
{
	enum Digits
	{
		Zero = 48,
		One,
		Two,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Nine
	};
	class DigitsForHUD
	{
		public void CreateText(string amount, TextMeshProUGUI textField)
		{
			Digits digit;
			for (int i = 0; i < amount.Length; i++)
			{
				int temp = Convert.ToInt32(amount[i]);
				digit = (Digits)temp;
				switch (digit)
				{
					case Digits.Zero:
						textField.text += "<sprite=" + @"""fonts""" + " index=0>"; //<sprite="fonts" index=0>
						break;
					case Digits.One:
						textField.text += "<sprite=" + @"""fonts""" + " index=1>";
						break;
					case Digits.Two:
						textField.text += "<sprite=" + @"""fonts""" + " index=2>";
						break;
					case Digits.Three:
						textField.text += "<sprite=" + @"""fonts""" + " index=3>";
						break;
					case Digits.Four:
						textField.text += "<sprite=" + @"""fonts""" + " index=4>";
						break;
					case Digits.Five:
						textField.text += "<sprite=" + @"""fonts""" + " index=5>";
						break;
					case Digits.Six:
						textField.text += "<sprite=" + @"""fonts""" + " index=6>";
						break;
					case Digits.Seven:
						textField.text += "<sprite=" + @"""fonts""" + " index=7>";
						break;
					case Digits.Eight:
						textField.text += "<sprite=" + @"""fonts""" + " index=8>";
						break;
					case Digits.Nine:
						textField.text += "<sprite=" + @"""fonts""" + " index=9>";
						break;
				}
			}
		}
	}
}

﻿using Assets.Scripts.Controllers;
using Assets.Scripts.Enemies;
using Assets.Scripts.Game;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Miscellanous;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Player
{
	public class PlayerController : MonoBehaviour, Damageable
	{
		[SerializeField] private int MaxHearts = 3;
		[SerializeField] private int CurrentHearts = 3;
		[SerializeField] private int Coins = 0;
		[SerializeField] private int OrbKeys = 0;
		[SerializeField] private float OnEnemyTouchKnockBackForce = 50f; //If player touches an enemy he will be knocked back with that force.

		[SerializeField] private Animator animator;
		// Use this for initialization

		private bool hasTakenDamage = false;
		[SerializeField] private float damageTakenTimer = 0.01f;
		private float timer;
		void Start()
		{
			CurrentHearts = MaxHearts;
			EventSystem.current.onCoinPickUp += OnCoinPickUp;
			EventSystem.current.onPlayerDamageTaken += OnDamageTaken;
			EventSystem.current.onHeastRestore += OnHeartRestore;
			EventSystem.current.onOrbPickUp += OnOrbPickUp;
			EventSystem.current.onOrbUse += OnOrbUse;
		}

		private void OnHeartRestore()
		{
			if (CurrentHearts < MaxHearts)
				CurrentHearts++;
		}

		// Update is called once per frame
		void Update()
		{
			if (hasTakenDamage)
				timer += Time.deltaTime;
			if (timer >= damageTakenTimer)
			{
				hasTakenDamage = false;
				timer = 0;
			}
		}
		 
		public void OnDamageTaken()
		{
			CurrentHearts -= 1;
			animator.SetTrigger("HitTaken");
			if (CurrentHearts == 0)
			{
				Die();
			}
		}

		void Die()
		{
			animator.SetBool("IsDead", true);
			EventSystem.current.PlayerDeath();
			StartCoroutine(DestroyThis());
		}

		IEnumerator DestroyThis()
		{
			yield return new WaitForSeconds(0.5f);
			Destroy(gameObject);
		}

		void OnCoinPickUp()
		{
			Coins++;
		}

		void OnOrbPickUp()
		{
			OrbKeys++;
		}

		void OnOrbUse()
		{
			OrbKeys--;
		}
		private void OnCollisionEnter2D(Collision2D collision)
		{
			if (collision.gameObject.GetComponent<EnemyController>() && !collision.gameObject.GetComponent<GoblinMovement>())
			{
				if (!hasTakenDamage)
				{
					EventSystem.current.PlayerDamageTaken();

					if (transform.position.x < collision.transform.position.x)
						GetComponent<Rigidbody2D>().AddForce(new Vector2(-OnEnemyTouchKnockBackForce, OnEnemyTouchKnockBackForce));
					else
						GetComponent<Rigidbody2D>().AddForce(new Vector2(OnEnemyTouchKnockBackForce, OnEnemyTouchKnockBackForce));

					hasTakenDamage = true;
				}
			}	
		}
		private void OnTriggerEnter2D(Collider2D collision)
		{
a			if (collision.gameObject.GetComponent<Door>())
			{
				var door = collision.gameObject.GetComponent<Door>();
				if (!door.GetIsOpened())
				{
					if (OrbKeys >= 1)
					{
						EventSystem.current.OrbUse();
						door.Open();
					}
				}
			}
		}

		public int GetCoinsAmount() { return Coins; }
		public int GetCurrentHearts() { return CurrentHearts; }
		public int GetMaxHearts() { return MaxHearts; }
		public int GetOrbKeysAmount() { return OrbKeys; }
	}
}
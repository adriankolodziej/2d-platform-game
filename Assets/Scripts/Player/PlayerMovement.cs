﻿using Assets.Scripts.Controllers;
using Assets.Scripts.Game;
using Assets.Scripts.Interfaces;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Player
{
	public class PlayerMovement : MonoBehaviour
	{
		[SerializeField] private CharacterController2D controller;
		[SerializeField] private Animator animator;
		[SerializeField] private float movementSpeed = 0;

		float horizontalInput = 0;
		float previousY = 0;
		float currentY = 0;

		bool jump = false;
		float attackCooldown = 1f;
		float attackTimer;


		private void Start()
		{
			currentY = gameObject.transform.position.y;
			previousY = currentY;
		}
		void Update()
		{
			horizontalInput = Input.GetAxisRaw("Horizontal") * movementSpeed;
			animator.SetFloat("Speed", Mathf.Abs(horizontalInput));

			currentY = gameObject.transform.position.y;
			animator.SetBool("IsFalling", IsFalling());
			previousY = currentY;

			if (Input.GetButtonDown("Jump"))
			{
				jump = true;
				animator.SetBool("IsJumping", true);
			}

			if (Input.GetButtonDown("Attack") && attackTimer >= attackCooldown)
			{
				animator.SetTrigger("Sword");
				controller.Attack();
				attackTimer = 0;
			}

			attackTimer += Time.deltaTime;
		}

		public void OnLanding()
		{
			animator.SetBool("IsJumping", false);
		}

		private void FixedUpdate()
		{
			controller.Move(horizontalInput * Time.fixedDeltaTime, jump);
			jump = false;
		}

		bool IsFalling()
		{
			if (currentY < previousY)
				if (Mathf.Abs(currentY - previousY) > 0.01)
					return true;
				else return false;
			else return false;
		}
	}
}
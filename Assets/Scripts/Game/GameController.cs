﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class GameController : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            EventSystem.current.onPlayerDeath += EndGame;
        }

        void EndGame()
		{
            StartCoroutine(ToMenu());
		}

        IEnumerator ToMenu()
		{
            yield return new WaitForSeconds(0.5f);
            Application.Quit();
		}
    }
}

﻿using System;
using UnityEngine;

namespace Assets.Scripts.Game
{
	public class EventSystem : MonoBehaviour
	{
		public static EventSystem current;
		void Awake()
		{
			current = this;
		}

		public event Action onCoinPickUp;
		public void CoinPickUp()
		{
			if(onCoinPickUp!=null)
			{
				onCoinPickUp();
			}
		}

		public event Action onOrbPickUp;
		public void OrbPickUp()
		{
			if (onOrbPickUp != null)
			{
				onOrbPickUp();
			}
		}

		public event Action onOrbUse;
		public void OrbUse()
		{
			if (onOrbUse != null)
			{
				onOrbUse();
			}
		}

		public event Action onPlayerDamageTaken;
		public void PlayerDamageTaken()
		{
			if(onPlayerDamageTaken != null)
			{
				onPlayerDamageTaken();
			}
		}

		public event Action onHeastRestore;
		public void HeartRestore()
		{
			if (onHeastRestore != null)
			{
				onHeastRestore();
			}
		}

		public event Action onPlayerDeath;
		public void PlayerDeath()
		{
			if (onPlayerDeath != null)
			{
				onPlayerDeath();
			}
		}
	}
}
﻿using Assets.Scripts.Controllers;
using Assets.Scripts.Interfaces;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
	public class EnemyController : MonoBehaviour, Damageable
	{
		[SerializeField] private int MaxHealth = 2;
		[SerializeField] private int CurrentHealth;
		[SerializeField] private Animator animator;
		[SerializeField] private EnemyController2D controller2D;
		void Start()
		{
			CurrentHealth = MaxHealth;
		}

		public void OnDamageTaken()
		{
			CurrentHealth--;
			animator.SetTrigger("HitTaken");
			if (CurrentHealth == 0)
			{
				Die();
			}
		}

		void Die()
		{
			animator.SetBool("IsWalking", false);
			animator.SetTrigger("Die");
			StartCoroutine(DestroyThis());
		}

		IEnumerator DestroyThis()
		{
			yield return new WaitForSeconds(0.3f);
			Destroy(transform.parent.gameObject);
		}
	}
}
﻿using Assets.Scripts.Controllers;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
	public class PatrollingEnemyMovement : MonoBehaviour
	{
		[SerializeField] protected EnemyController2D controller;
		[SerializeField] protected Animator animator;
		[SerializeField] protected float movementSpeed = 0;
		[SerializeField] protected Transform patrolPoint;
		[SerializeField] protected bool isPatrolling;
		protected Vector3 startingPosition;
		protected float horizontalInput = 0;
		protected int direction = 1;
		protected bool initialValueOfIsPatrolling;
		protected void Start()
		{
			initialValueOfIsPatrolling = isPatrolling;
			startingPosition = transform.position;
		}
		void Update()
		{
			if (!isPatrolling)
			{
				animator.SetBool("IsWalking", false);
				horizontalInput = 0;
			}
			else
			{
				CheckDirection();
				horizontalInput = direction * movementSpeed;
				animator.SetBool("IsWalking", true);
			}

		}

		private void FixedUpdate()
		{
			controller.Move(horizontalInput * Time.fixedDeltaTime);
		}

		protected void CheckDirection()
		{
			if (direction == 1)
			{
				if (transform.position.x >= patrolPoint.position.x)
					direction *= -1;
			}
			else if (direction == -1)
			{
				if (transform.position.x <= startingPosition.x)
					direction *= -1;
			}
		}
	}
}
﻿using Assets.Scripts.Player;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
	public class GoblinMovement : PatrollingEnemyMovement
	{
		[SerializeField] private float attackCooldown = 1.5f;
		[SerializeField] private float playerDetectionRange = 0.1f;
		private float attackTimer;
		void Update()
		{
			if (!isPatrolling)
			{
				animator.SetBool("IsWalking", false);
				horizontalInput = 0;
			}
			else
			{
				CheckDirection();
				horizontalInput = direction * movementSpeed;
				animator.SetBool("IsWalking", true);
			}


			if (attackTimer >= attackCooldown)
			{
				int direction = 1;
				Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, playerDetectionRange);
				for (int i = 0; i < colliders.Length; i++)
				{
					if (colliders[i].gameObject.GetComponent<PlayerController>())
					{
						if (colliders[i].transform.position.x < transform.position.x)
							direction = -1;
						else direction = 1;
						isPatrolling = false;
						animator.SetTrigger("Attack");
						StartCoroutine(PerformAttack(direction));
						attackTimer = 0;
					}
				}
			}
			if (!isPatrolling && attackTimer < attackCooldown)
			{
				horizontalInput = 0;
			}
			attackTimer += Time.deltaTime;
		}

		private void FixedUpdate()
		{
			controller.Move(horizontalInput * Time.fixedDeltaTime);
		}

		IEnumerator PerformAttack(int direction)
		{
			yield return new WaitForSeconds(0.3f);
			controller.Attack(direction);
			isPatrolling = initialValueOfIsPatrolling;
		}
	}
}